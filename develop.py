#!/usr/bin/python3

import logging
import os
import sys

sys.path.insert(1, os.path.join(os.path.dirname(__file__), 'src'))
for dep in ('Flask-OAuthlib-0.9.4',):
    sys.path.insert(1, os.path.join(os.path.dirname(__file__),
                                    'lib', dep))

from gitlab_self_service.flask_app import app as application


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    application.run(debug=True)

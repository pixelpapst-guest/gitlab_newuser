#!/usr/bin/python3
#######################################################################
#
# call_gitlab_api.py - functions for calling into the gitlab API, given
#                      an appropriate autorization token
#
# Copyright (C) 2017 Philipp Kaluza
# Copyright (C) 2017 Nicolas Dandrimont
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

import json
from urllib.parse import urljoin, quote as urlquote

import requests


def ppjson(jsonobj):
    print(json.dumps(jsonobj, sort_keys=True, indent=4))


def prettify_response(resp):
    pretty = json.dumps(resp.json(), sort_keys=True, indent=4)
    return pretty


class GitLabAPIError(Exception):
    """An error raised by the GitLab API."""
    @classmethod
    def from_response(cls, base_message, response):
        """Raise an api error from the response returned by the API."""

        try:
            message = response.json()['message']
            data = message
        except Exception:
            message = ("Failed to decode API response. Status code %s, "
                       "raw message: %s" % (response.status_code,
                                            response.text))
            data = None

        raise cls('%s: %s' % (base_message, message), data)


class GitLabAPIClient:
    """A (bare-bones) client for the GitLab API.

    Arguments:
      url (str): the base URL for the API. Should probably end with '/api/v4/'
      token (str): the authentication token for the API
      mode (str): either 'impersonate' (use an impersonation token) or 'oauth2'
        (use an oauth2 client token).
      ca_bundle (str): path to the CA certificate bundle used to verify the
        HTTPS connections.
    """
    def __init__(self, url, token, mode):
        self.url = url
        self.token = token

        if mode not in ('impersonate', 'oauth2'):
            raise NotImplementedError('Unknown API client mode %s' % mode)

        self.mode = mode

    def get_headers(self):
        """Return the headers needed to make queries to the GitLab API"""
        if self.mode == 'impersonate':
            return {
                'PRIVATE-TOKEN': self.token,
            }
        elif self.mode == 'oauth2':
            return {
                'Authorization': 'Bearer %s' % self.token,
            }

    def api_request(self, *, path, params=None, method='GET'):
        """Make a request to the GitLab API

        Arguments:
          path (str): the path in the API we want to make the request for
          params (dict): the parameters to be sent to the API
          method (str): either 'GET' or 'POST'

        Returns:
          requests.Response: the raw response received from the API
        """
        full_url = urljoin(self.url, path)
        headers = self.get_headers()
        if not params:
            params = {}

        if method == 'GET':
            resp = requests.get(full_url, headers=headers)
        elif method == 'POST':
            resp = requests.post(full_url, json=params, headers=headers)
        elif method == 'DELETE':
            resp = requests.delete(full_url, headers=headers)
        else:
            raise NotImplementedError('Unsupported request method %s' % method)

        return resp

    def get_user(self):
        """Get information about the current user"""
        return self.api_request(path='user').json()

    def list_projects(self):
        """List the projects from the GitLab instance.

        Returns:
          list: the list of projects hosted by the GitLab instance.
        """
        return self.api_request(path='projects').json()

    def get_group(self, path):
        """Get information about a group

        Arguments:
          path (str or int): the path of the searched group, or its id.

        Returns:
          dict: the information about the given group
        """

        resp = self.api_request(path='groups/%s' % urlquote(str(path)))

        if resp.status_code != 200:
            raise GitLabAPIError("Group not found: %s" % path)

        return resp.json()

    def get_group_members(self, *, group):
        """List the members of the group

        Mandatory arguments:
          group (str or int): id of the group to list

        Returns:
          list: list of user access entries
        """

        resp = self.api_request(
            path='groups/%s/members' % urlquote(str(group))
        )

        if resp.status_code != 200:
            raise GitLabAPIError("Group not found: %s" % group)

        return resp.json()

    def create_new_user(self, *, username, password, name, email, **kwargs):
        """Create a new user through the GitLab API.

        Mandatory Arguments:
          username (str): the username of the new user
          password (str): the password for the new user
          name (str): the full name of the new user
          email (str): the email address of the new user
        Other arguments:
          See documentation at https://docs.gitlab.com/ce/api/users.html#user-creation

        Returns:
          dict: the decoded response from the GitLab API

        Raises:
          GitLabAPIError when the user creation failed
        """
        params = {
            'email': email,
            'password': password,
            'username': username,
            'name': name,
        }

        params.update(kwargs)

        ret = self.api_request(path='users', params=params, method='POST')
        if ret.status_code == 201:
            return ret.json()

        raise GitLabAPIError.from_response('User creation failed', ret)

    def create_new_group(self, *, name, path, **kwargs):
        """Create a new group

        Mandatory Arguments:
          name (str): the name of the new group
          path (str): the path for the new group
        Other arguments:
          See documentation at https://docs.gitlab.com/ce/api/groups.html#new-group

        Returns:
          dict: the decoded response from the GitLab API

        Raises:
          GitLabAPIError when the group creation failed
        """
        params = {
            'name': name,
            'path': path,
        }

        params.update(kwargs)

        ret = self.api_request(path='groups', params=params, method='POST')
        if ret.status_code == 201:
            return ret.json()

        raise GitLabAPIError.from_response('Group creation failed', ret)

    def add_group_member(self, *, group, user_id, access_level, **kwargs):
        """Add a member to a group.

        Mandatory arguments:
          group (str or int): id of the group
          user_id (int): id of the user to add to the group
          access_level (int): access level to add the user at. Valid access
              levels::
            10: Guest access
            20: Reporter access
            30: Developer access
            40: Master access
            50: Owner access
        Other Arguments:
          see documentation at https://docs.gitlab.com/ce/api/members.html#add-a-member-to-a-group-or-project

        Returns:
          dict: the decoded response from the GitLab API
        Raises:
          GitLabAPIError when the group member addition failed
        """

        params = {
            'user_id': user_id,
            'access_level': access_level,
        }
        params.update(kwargs)

        resp = self.api_request(
            path='groups/%s/members' % urlquote(str(group)),
            params=params,
            method='POST',
        )

        if resp.status_code == 201:
            return resp.json()

        raise GitLabAPIError.from_response('Group membership addition failed',
                                           resp)

    def delete_group_member(self, *, group, user_id):
        """Add a member to a group.

        Mandatory arguments:
          group (str or int): id of the group
          user_id (int): id of the user to remove from the group

        Returns:
          bool: True if the user was removed
        Raises:
          GitLabAPIError when the group member removal failed
        """

        resp = self.api_request(
            path='groups/%s/members/%s' % (urlquote(str(group)), user_id),
            method='DELETE',
        )

        if resp.status_code == 204:
            return True

        raise GitLabAPIError.from_response('Group membership removal failed',
                                           resp)
